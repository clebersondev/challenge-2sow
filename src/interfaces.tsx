export interface FormUser {
  name: string;
  cpf: string;
  email: string;
  cep: string;
  street: string;
  number: number;
  neighborhood: string;
  city: string;
}

export interface ServerUser {
  id: string;
  nome: string;
  cpf: string;
  email: string;
  endereco: {
    cep: number;
    rua: string;
    numero: number;
    bairro: string;
    cidade: string;
  }
}