import React from "react";
import { Container, Divider } from "semantic-ui-react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import HomePage from "./pages/Home";
import LoginPage from "./pages/Login";
import CreateUserPage from "./pages/CreateUser";
import { AuthProvider, useAuth } from "./auth";
import { ToastProvider } from "react-toast-notifications";
import PrivateRoute from "./components/PrivateRoute";
import Navbar from "./components/Navbar";

function App() {
  const { authToken, login, logout, isAuthenticated } = useAuth();

  return (
    <ToastProvider
      placement="bottom-center"
      autoDismiss
    >
      <AuthProvider value={{ authToken, login, logout, isAuthenticated }}>
        <Router>
          <div>
            <Navbar />
            <Divider />
            <Container style={{ padding: "5rem 0" }}>
              <Switch>
                <PrivateRoute exact path="/">
                  <HomePage />
                </PrivateRoute>
                <Route exact path="/entrar" component={LoginPage} />
                <PrivateRoute exact path="/criar">
                  <CreateUserPage />
                </PrivateRoute>
              </Switch>
            </Container>
          </div>
        </Router>
      </AuthProvider>
    </ToastProvider>
  );
}

export default App;
