import React, { useEffect, useRef, useState } from "react";
import { SubmitHandler, useForm, ValidationRules } from "react-hook-form";
import NumberFormat from "react-number-format";
import { Button, Dimmer, Form, Loader, Message } from "semantic-ui-react";
import axios from "axios";
import { FormUser } from "../interfaces";

interface Props {
  onSubmit: SubmitHandler<FormUser>;
  initialValues?: FormUser;
  submitBtnText?: string;
}

const defaultValues: FormUser = {
  name: "",
  cep: "",
  city: "",
  cpf: "",
  email: "",
  neighborhood: "",
  number: 0,
  street: "",
};

const UserForm: React.FC<Props> = ({
  onSubmit,
  initialValues = defaultValues,
  submitBtnText = "Enviar",
}) => {
  const { register, handleSubmit, errors, setValue } = useForm({
    defaultValues: initialValues,
  });

  const [loadingCep, setLoadingCep] = useState(false);
  const numberInputRef = useRef<HTMLInputElement>(null);

  const fields: Record<string, ValidationRules> = {
    name: {
      required: "Nome obrigatório",
    },
    cpf: {
      required: "CPF obrigatório",
      pattern: {
        value: /\d{3}\.\d{3}\.\d{3}\-\d{2}/,
        message: "CPF inválido",
      },
    },
    email: {
      required: "E-mail obrigatório",
    },
    cep: {
      required: "CEP obrigatório",
    },
    street: {
      required: "Rua obrigatória",
    },
    number: {
      required: "Número obrigatório",
      min: {
        value: 0,
        message: "Número deve ser positivo (maior que 0)",
      },
    },
    neighborhood: {
      required: "Bairro obrigatório",
    },
    city: {
      required: "Cidade obrigatória",
    },
  };

  useEffect(() => {
    register({ name: "number" }, fields.number);
  }, []);

  return (
    <>
      {loadingCep && (
        <Dimmer active>
          <Loader />
        </Dimmer>
      )}
      <Form onSubmit={handleSubmit(onSubmit)} error={Boolean(errors)}>
        <Form.Field error={Boolean(errors.name)}>
          <label>Nome</label>
          <input name="name" ref={register(fields.name)} />
        </Form.Field>
        <Form.Field error={Boolean(errors.cpf)}>
          <label>CPF</label>
          <NumberFormat
            name="cpf"
            format="###.###.###-##"
            getInputRef={register(fields.cpf)}
          />
        </Form.Field>
        <Form.Field error={Boolean(errors.email)}>
          <label>E-mail</label>
          <input name="email" type="email" ref={register(fields.email)} />
        </Form.Field>
        <Form.Field error={Boolean(errors.cep)}>
          <label>CEP</label>
          <NumberFormat
            name="cep"
            format="#####-###"
            getInputRef={register(fields.cep)}
            onChange={(e) => {
              const cepRegexp = /\d{5}\-\d{3}/;
              if (!cepRegexp.test(e.target.value)) return;

              setLoadingCep(true);
              const cep = e.target.value;
              axios
                .get(`https://viacep.com.br/ws/${cep}/json/`)
                .then(({ data }) => {
                  setValue("street", data.logradouro);
                  setValue("city", data.localidade);
                  setValue("neighborhood", data.bairro);
                  numberInputRef?.current?.focus();
                })
                .catch(console.error)
                .finally(() => {
                  setLoadingCep(false);
                });
            }}
          />
        </Form.Field>
        <Form.Field error={Boolean(errors.street)}>
          <label>Rua</label>
          <input name="street" ref={register(fields.street)} />
        </Form.Field>
        <Form.Field error={Boolean(errors.number)}>
          <label>Número</label>
          <input
            name="number"
            type="number"
            ref={numberInputRef}
            defaultValue={initialValues.number}
            onChange={(e) => {
              setValue("number", Number(e.target.value));
            }}
          />
        </Form.Field>
        <Form.Field error={Boolean(errors.neighborhood)}>
          <label>Bairro</label>
          <input name="neighborhood" ref={register(fields.neighborhood)} />
        </Form.Field>
        <Form.Field error={Boolean(errors.city)}>
          <label>Cidade</label>
          <input name="city" ref={register(fields.city)} />
        </Form.Field>
        {Boolean(errors) && (
          <Message
            error
            content={
              errors.name?.message ||
              errors.cpf?.message ||
              errors.email?.message ||
              errors.cep?.message ||
              errors.street?.message ||
              errors.number?.message ||
              errors.neighborhood?.message ||
              errors.city?.message
            }
          />
        )}
        <Button primary type="submit">
          {submitBtnText}
        </Button>
      </Form>
    </>
  );
};

export default UserForm;
