import React, { useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import { Container, Icon, Menu } from "semantic-ui-react";
import { AuthContext } from "../auth";

const Navbar: React.FC = () => {
  const history = useHistory();
  const { isAuthenticated, logout } = useContext(AuthContext);

  return (
    <Menu fixed="top" inverted>
      <Container>
        <Menu.Item header>
          <Icon name="user" size="large" />
          User Store
        </Menu.Item>
        {isAuthenticated() ? (
          <>
            <Menu.Item as={Link} to="/">
              Usuários
            </Menu.Item>
            <Menu.Item as={Link} to="/criar">
              Criar usuário
            </Menu.Item>
            <Menu.Item
              onClick={() => {
                logout();
                history.push("/entrar");
              }}
            >
              Sair
            </Menu.Item>
          </>
        ) : (
          <Menu.Item as={Link} to="/entrar">
            Entrar
          </Menu.Item>
        )}
      </Container>
    </Menu>
  );
};

export default Navbar;
