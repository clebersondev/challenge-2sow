import React, { useContext } from "react";
import { Redirect, Route, RouteProps } from "react-router-dom";
import { AuthContext } from "../auth";

const PrivateRoute: React.FC<RouteProps> = ({ children, ...props }) => {
  const { isAuthenticated } = useContext(AuthContext);

  return (
    <Route
      {...props}
      render={({ location }) =>
        isAuthenticated() ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/entrar",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
};

export default PrivateRoute;
