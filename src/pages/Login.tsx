import React, { useContext, useEffect } from "react";
import { Container, Form, Button, Message, Header } from "semantic-ui-react";
import { SubmitHandler, useForm, ValidationRules } from "react-hook-form";
import { AuthContext } from "../auth";
import { useHistory } from "react-router-dom";
import { useToasts } from "react-toast-notifications";
import { FormUser } from "../interfaces";

const LoginPage: React.FC = () => {
  const { login, isAuthenticated } = useContext(AuthContext);
  const history = useHistory();
  const { addToast } = useToasts();

  useEffect(() => {
    addToast(`Para fazer uso do sistema, você deve-se logar primeiro.`, {
      appearance: "info",
    });
  }, []);

  if (isAuthenticated()) history.replace("/");

  const { register, handleSubmit, errors } = useForm();

  const fields: Record<string, ValidationRules> = {
    email: {
      required: "E-mail obrigatório.",
    },
    password: {
      required: "Senha obrigatória.",
      minLength: {
        value: 4,
        message: "A senha deve ter no mínimo 4 caracteres.",
      },
    },
  };

  const onSubmit: SubmitHandler<FormUser> = (data) => {
    const [emailName] = data.email.split("@");

    login();
    history.replace("/");

    addToast(`Bem-vindo, ${emailName}!`, { appearance: "info" });
  };

  return (
    <Container text>
      <Header as="h1" dividing>
        Login
      </Header>
      <Form onSubmit={handleSubmit(onSubmit)} error={Boolean(errors)}>
        <Form.Field error={Boolean(errors.email)}>
          <label>E-mail{fields.email.required && "*"}</label>
          <input type="email" name="email" ref={register(fields.email)} />
        </Form.Field>
        <Form.Field error={Boolean(errors.password)}>
          <label>Senha{fields.password.required && "*"}</label>
          <input
            type="password"
            name="password"
            ref={register(fields.password)}
          />
        </Form.Field>
        {Boolean(errors) && (
          <Message
            error
            content={errors.email?.message || errors.password?.message}
          />
        )}
        <Button primary type="submit">
          Entrar
        </Button>
      </Form>
    </Container>
  );
};

export default LoginPage;
