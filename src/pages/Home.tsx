import React, { useEffect, useState } from "react";
import axios from "axios";
import {
  Container,
  Table,
  Button,
  Form,
  Modal,
  Loader,
  Dimmer,
} from "semantic-ui-react";
import UserForm from "../components/UserForm";
import { ServerUser } from "../interfaces";
import { useToasts } from "react-toast-notifications";

const HomePage: React.FC = () => {
  const [users, setUsers] = useState<ServerUser[]>([]);
  const [query, setQuery] = useState("");
  const [loading, setLoading] = useState(false);

  const [userToUpdate, setUserToUpdate] = useState<ServerUser | null>(null);
  const [userToDelete, setUserToDelete] = useState<string>("");

  const { addToast } = useToasts();

  useEffect(() => {
    axios
      .get(`/users?q=${query}`)
      .then((res) => {
        setUsers(res.data);
      })
      .catch(() => {
        addToast(
          "Ocorreu um erro de comunicação com servidor. Volte mais tarde.",
          { appearance: "error" }
        );
      });
  }, [query]);

  return (
    <Container>
      {loading && (
        <Dimmer active page>
          <Loader />
        </Dimmer>
      )}
      <Form>
        <Form.Field>
          <label>Buscar usuário</label>
          <input value={query} onChange={(e) => setQuery(e.target.value)} />
        </Form.Field>
      </Form>
      {users.length > 0 && (
        <Table celled>
          <Table.Header>
            <Table.HeaderCell>Nome</Table.HeaderCell>
            <Table.HeaderCell>CPF</Table.HeaderCell>
            <Table.HeaderCell>E-mail</Table.HeaderCell>
            <Table.HeaderCell>Cidade</Table.HeaderCell>
            <Table.HeaderCell>Ações</Table.HeaderCell>
          </Table.Header>
          <Table.Body>
            {users.map((user) => (
              <Table.Row key={user.id}>
                <Table.Cell>{user.nome}</Table.Cell>
                <Table.Cell>{user.cpf}</Table.Cell>
                <Table.Cell>{user.email}</Table.Cell>
                <Table.Cell>{user.endereco.cidade}</Table.Cell>
                <Table.Cell>
                  <Button
                    icon="edit"
                    color="blue"
                    onClick={() => setUserToUpdate(user)}
                  />
                  <Button
                    icon="delete"
                    color="red"
                    onClick={() => {
                      setUserToDelete(user.id);
                    }}
                  />
                </Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      )}

      {userToUpdate && (
        <Modal open onClose={() => setUserToUpdate(null)}>
          {loading && (
            <Dimmer active>
              <Loader />
            </Dimmer>
          )}
          <Modal.Header>Editar usuário</Modal.Header>
          <Modal.Content>
            <UserForm
              initialValues={{
                name: userToUpdate.nome,
                cpf: userToUpdate.cpf,
                email: userToUpdate.email,
                cep: String(userToUpdate.endereco.cep),
                city: userToUpdate.endereco.cidade,
                street: userToUpdate.endereco.rua,
                neighborhood: userToUpdate.endereco.bairro,
                number: userToUpdate.endereco.numero,
              }}
              submitBtnText="Atualizar"
              onSubmit={(data) => {
                setLoading(true);
                axios
                  .patch(`/users/${userToUpdate.id}`, {
                    nome: data.name,
                    cpf: data.cpf,
                    email: data.email,
                    endereco: {
                      cep: data.cep,
                      rua: data.street,
                      numero: data.number,
                      bairro: data.neighborhood,
                      cidade: data.city,
                    },
                  })
                  .then((res) => {
                    addToast("Usuário editado com sucesso", {
                      appearance: "success",
                    });
                    setUsers(
                      users.map((other) =>
                        other.id === res.data.id ? res.data : other
                      )
                    );
                  })
                  .catch((err) => {
                    console.error(err);
                  })
                  .finally(() => {
                    setLoading(false);
                    setUserToUpdate(null);
                  });
              }}
            />
          </Modal.Content>
        </Modal>
      )}

      {userToDelete && (
        <Modal open onClose={() => setUserToDelete("")}>
          <Modal.Header>Remover usuário</Modal.Header>
          <Modal.Content>
            <p>Tem certeza de remover o usuário?</p>
          </Modal.Content>
          <Modal.Actions>
            <Button
              labelPosition="right"
              content="Sim, quero remover"
              icon="delete"
              negative
              onClick={() => {
                setLoading(true);
                axios
                  .delete(`/users/${userToDelete}`)
                  .then(() => {
                    addToast("Usuário removido com sucesso", {
                      appearance: "success",
                    });
                    setUsers(
                      users.filter((other) => other.id !== userToDelete)
                    );
                  })
                  .catch(() => {
                    addToast("Algo deu errado =(. Tente novamente.", {
                      appearance: "error",
                    });
                  })
                  .finally(() => {
                    setLoading(false);
                    setUserToDelete("");
                  });
              }}
            />
          </Modal.Actions>
        </Modal>
      )}
    </Container>
  );
};

export default HomePage;
