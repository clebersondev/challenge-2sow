import React, { useState } from "react";
import axios from "axios";
import { Container, Dimmer, Header, Loader } from "semantic-ui-react";
import { v4 as uuid } from "uuid";
import UserForm from "../components/UserForm";
import { useHistory } from "react-router-dom";
import { useToasts } from "react-toast-notifications";

const CreateUserPage: React.FC = () => {
  const [loading, setLoading] = useState(false);

  const history = useHistory();
  const { addToast } = useToasts();

  return (
    <Container>
      {loading && (
        <Dimmer active page>
          <Loader />
        </Dimmer>
      )}
      <Header as="h1" dividing>
        Criar usuário
      </Header>
      <UserForm
        submitBtnText="Criar"
        onSubmit={(data) => {
          setLoading(true);

          axios
            .post("/users", {
              id: uuid(),
              nome: data.name,
              cpf: data.cpf,
              email: data.email,
              endereco: {
                cep: data.cep,
                rua: data.street,
                numero: data.number,
                bairro: data.neighborhood,
                cidade: data.city,
              },
            })
            .then(() => {
              addToast("Usuário criado com sucesso.", {
                appearance: "success",
              });
              history.push("/");
            })
            .catch(() => {
              addToast("Algo deu errado =(. Tente novamente.", {
                appearance: "error",
              });
            })
            .finally(() => {
              setLoading(false);
            });
        }}
      />
    </Container>
  );
};

export default CreateUserPage;
