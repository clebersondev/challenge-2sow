import React, { useState } from "react";
import { v4 as uuid } from "uuid";

export function useAuth() {
  const KEY = "authToken";

  const [authToken, setAuthToken] = useState(() =>
    window.localStorage.getItem(KEY)
  );

  return {
    authToken,
    isAuthenticated: () => authToken !== null,
    login() {
      if (authToken) return;

      const token = uuid();
      window.localStorage.setItem(KEY, token);
      setAuthToken(token);
    },
    logout() {
      window.localStorage.removeItem(KEY);
      setAuthToken(null);
    },
  };
}

export type AuthContextType = {
  authToken: string | null;
  login: () => void;
  logout: () => void;
  isAuthenticated: () => boolean;
}

export const AuthContext = React.createContext<AuthContextType>({
  authToken: null,
  login: () => {},
  logout: () => { },
  isAuthenticated: () => true,
});
export const AuthProvider = AuthContext.Provider;
export const AuthConsumer = AuthContext.Consumer;
