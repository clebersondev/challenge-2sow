# Desafio 2SOW

Sistema para cadastro de usuários feito como [teste](https://2sow.github.io/vaga-frontend/) para a vaga de desenvolvedor front-end/mobile na 2SOW.



## 🔧 Instalação e Uso

Para funcionar, é necessário que tenha [Node.js](https://nodejs.org/) instalado em sua máquina. Uma vez instalado:

- Baixe o código-fonte do repositório.

- Dentro do diretório, execute o comando na sua linha de comandos:

  ``````bash
  npm run start
  ``````

- Isso iniciará a API fake em `http://localhost:5000` e o servidor de desenvolvimento da aplicação React em `http://localhost:3000`.